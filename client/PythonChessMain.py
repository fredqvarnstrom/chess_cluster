#! /usr/bin/env python
from ChessBoard import ChessBoard
from ChessPlayer import ChessPlayer
from ChessGUI_pygame import ChessGUI_pygame
from ChessRules import ChessRules

import sys
import Pyro4


class PythonChessMain:
    ser_ip = None

    def __init__(self, ser_ip):
        # 0 : for normal board setup; see ChessBoard class for other options (for testing purposes)
        self.Board = ChessBoard(0)
        self.Rules = ChessRules()
        self.ser_ip = ser_ip

    def SetUp(self):
        # gameSetupParams: Player 1 and 2 Name, Color, Human/AI level

        player1Name = 'Shaaz'
        player1Type = 'human'
        player1Color = 'white'
        player2Name = 'AI'
        player2Type = 'defenseAI'   # 'offenseAI'
        player2Color = 'black'

        self.player = [0, 0]
        self.player[0] = ChessPlayer(player1Name,player1Color)

        uri = None
        if player2Type == 'randomAI':
            uri = "PYRO:ran_ai@" + self.ser_ip + ":1234"
        elif player2Type == 'defenseAI':
            uri = "PYRO:def_ai@" + self.ser_ip + ":1234"
        elif player2Type == 'offenseAI':
            uri = "PYRO:off_ai@" + self.ser_ip + ":1234"

        self.player[1] = Pyro4.Proxy(uri)

        self.AIvsAI = False
            
        self.AIpause = False
            
        # create the gui object - didn't do earlier because pygame conflicts with any gui manager (Tkinter, WxPython...)
        self.guitype = 'pygame'
        self.Gui = ChessGUI_pygame(1)
            
    def MainLoop(self):
        currentPlayerIndex = 0
        turnCount = 0
        while True:
            board = self.Board.GetState()
            p_color = self.player[currentPlayerIndex].GetColor()

            if self.Rules.IsCheckmate(board, p_color):
                break

            p_name = self.player[currentPlayerIndex].GetName()
            # hardcoded so that player 1 is always white
            if p_color == 'white':
                turnCount = turnCount + 1

            self.Gui.PrintMessage("")
            baseMsg = "TURN %s - %s (%s)" % (str(turnCount), p_name, p_color)
            self.Gui.PrintMessage("-----%s-----" % baseMsg)
            self.Gui.Draw(board)
            if self.Rules.IsInCheck(board, p_color):
                self.Gui.PrintMessage("Warning..." + p_name + " (" + p_color + ") is in check!")
            
            if self.player[currentPlayerIndex].GetType() == 'AI':
                moveTuple = self.player[currentPlayerIndex].GetMove(self.Board.GetState(), p_color)
            else:
                moveTuple = self.Gui.GetPlayerInput(board, p_color)

            # moveReport = string like "White Bishop moves from A1 to C3" (+) "and captures ___!"
            moveReport = self.Board.MovePiece(moveTuple)
            self.Gui.PrintMessage(moveReport)

            # this will cause the currentPlayerIndex to toggle between 1 and 0
            currentPlayerIndex = (currentPlayerIndex+1)%2
        
        self.Gui.PrintMessage("CHECKMATE!")
        winnerIndex = (currentPlayerIndex+1) % 2
        self.Gui.PrintMessage(self.player[winnerIndex].GetName()+" ("+self.player[winnerIndex].GetColor()+") won the game!")
        self.Gui.EndGame(board)

if __name__ == "__main__":

    print sys.argv
    if len(sys.argv) > 1:
        server_ip = sys.argv[1]
    else:
        server_ip = "192.168.1.120"

    game = PythonChessMain(server_ip)
    game.SetUp()
    game.MainLoop()


    