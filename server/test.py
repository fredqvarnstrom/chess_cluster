import socket, fcntl, struct

def get_local_ip(ifname):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        addr = socket.inet_ntoa(fcntl.ioctl(s.fileno(), 0x8915, struct.pack('256s', ifname[:15]))[20:24])
    except IOError:
        addr = None
    return addr

print get_local_ip("eth0")
print get_local_ip("wlan0")